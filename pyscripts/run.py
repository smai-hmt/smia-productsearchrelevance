import numpy as np
import pandas as pd
from genFeat_distance_feat import *
from genFeat_counting_feat import *
import sys
import cPickle
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor

sys.path.append('../pyscripts/')

# Read the csv files
df_train = pd.read_csv('../data/train.csv')
#df_test = pd.read_csv('../data/test.csv')
df_attr = pd.read_csv('../data/attributes.csv')
df_pro_desc = pd.read_csv('../data/product_descriptions.csv')


''' Do basic preproccesing, then go for advanced '''

# Merge both training and testing dataframes to do preprocessing on both
# df_all = pd.concat((df_train, df_test), axis=0, ignore_index=True)

# Merge (Left outer join) product description
df_train = pd.merge(df_train, df_pro_desc, how='left', on='product_uid')
df_train = df_train[:1000]

''' Extract basic distance features '''
#run_dist_features(df_train.copy(deep=True),df_test.copy(deep=True))
run_counting_features(df_train.copy(deep=True),'train_count')

'''
df_train = pd.read_csv('../data/train.csv')
df_test = pd.read_csv('../data/test.csv')

target = df_train['relevance'].values[:1000]
print 'target',target.shape
id_test = df_test['id'][:1000]
print id_test

df_train_count = pd.read_csv('counting_dfTrain')
df_train_distance = pd.read_csv('distance_dfTrain')


df_test_count = pd.read_csv('counting_dfTest')
df_test_distance = pd.read_csv('distance_dfTest')


X_train = pd.concat([df_train_count,df_train_distance],axis=1)
X_test = pd.concat([df_test_count,df_test_distance],axis=1)

print X_train.shape
print X_test.shape
print target.shape
print id_test.shape

rf = RandomForestRegressor(n_estimators=15, max_depth=6, random_state=0)
clf = BaggingRegressor(rf, n_estimators=45, max_samples=0.1, random_state=25)

clf.fit(X_train, target)

y_pred = clf.predict(X_test)
pd.DataFrame({"id": id_test, "relevance": y_pred}).to_csv('submission.csv',index=False)
'''
