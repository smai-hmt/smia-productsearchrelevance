
# coding: utf-8

# In[14]:

import pandas as pd
from sklearn.cross_validation import train_test_split
from math import sqrt
from sklearn.metrics import mean_squared_error

    
df_train = pd.read_csv('trainCount.csv',header=None)
df_test = pd.read_csv('testCount.csv',header=None)

df_train2 = pd.read_csv('trainDist.csv',header=None)
df_test2 = pd.read_csv('testDist.csv',header=None)


df_train = pd.concat([df_train,df_train2],axis =1)
df_test = pd.concat([df_test,df_test2],axis=1)

#Rename the columns
df_train.columns = range(0,len(df_train.columns))
df_test.columns = range(0,len(df_test.columns))


print df_train.shape
print df_test.shape

df_train_ori = pd.read_csv('../data/train.csv')
df_test_ori = pd.read_csv('../data/test.csv')

df_test_id = df_test_ori['id'].astype(int)
y = df_train_ori['relevance']





# In[15]:

print df_train.shape
print df_test.shape
print df_test_id.shape


# In[18]:

from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
rf = RandomForestRegressor(n_estimators=15, max_depth=6, random_state=0)
clf = BaggingRegressor(rf, n_estimators=45, max_samples=0.1, random_state=25)

X_train, X_test, y_train, y_test = train_test_split(df_train, y, test_size=0.2, random_state=1234)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

rms=sqrt(mean_squared_error(y_test, y_pred))
print "\tScore {0}\n\n".format(rms)

for i in range(len(y_pred)):
    if y_pred[i]<1.0:
        y_pred[i] = 1.0
    if y_pred[i]>3.0:
        y_pred[i] = 3.0

pd.DataFrame({"id": df_test_id.values.ravel(), "relevance": y_pred}).to_csv('submission.csv',index=False)
