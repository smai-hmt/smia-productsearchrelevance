# coding: utf-8

# In[14]:

import pandas as pd
from sklearn.cross_validation import train_test_split
from math import sqrt
from sklearn.metrics import mean_squared_error
from hyperopt import fmin, tpe, hp, STATUS_OK, Trials
import xgboost as xgb
from hyperopt import hp



def score(params):
    print "Training with params : "
    print params
    #num_round = int(params['n_estimators'])
    #del params['n_estimators']
    params['max_depth']=int(params['max_depth'])
    dtrain = xgb.DMatrix(X_train, label=y_train)
    dvalid = xgb.DMatrix(X_test, label=y_test)
    # watchlist = [(dvalid, 'eval'), (dtrain, 'train')]
    model = xgb.train(params, dtrain)
    pred = model.predict(dvalid)
    for i in range(len(pred)):
	    if pred[i]<1.0:
	        pred[i] = 1.0
	    if pred[i]>3.0:
	        pred[i] = 3.0
    rms=sqrt(mean_squared_error(y_test, pred))
    print "\tScore {0}\n\n".format(rms)
    return {'loss': rms, 'status': STATUS_OK}





    
df_train = pd.read_csv('trainCount.csv',header=None)
df_test = pd.read_csv('testCount.csv',header=None)

df_train2 = pd.read_csv('trainDist.csv',header=None)
df_test2 = pd.read_csv('testDist.csv',header=None)


df_train = pd.concat([df_train,df_train2],axis =1)
df_test = pd.concat([df_test,df_test2],axis=1)

#Rename the columns
df_train.columns = range(0,len(df_train.columns))
df_test.columns = range(0,len(df_test.columns))


print df_train.shape
print df_test.shape

df_train_ori = pd.read_csv('../data/train.csv')
df_test_ori = pd.read_csv('../data/test.csv')

df_test_id = df_test_ori['id'].astype(int)
y = df_train_ori['relevance']



# In[15]:

print df_train.shape
print df_test.shape
print df_test_id.shape


# In[18]:


params = {
    'task': 'regression',
    'booster': 'gbtree',
    'objective': 'reg:linear',
    'eta': hp.quniform('eta', 0.01, 1, 0.01),
    'gamma': hp.quniform('gamma', 0, 2, 0.1),
    'min_child_weight': hp.quniform('min_child_weight', 0, 10, 1),
    'max_depth': hp.quniform('max_depth', 1, 10, 1),
    'subsample': hp.quniform('subsample', 0.5, 1, 0.1),
    'colsample_bytree': hp.quniform('colsample_bytree', 0.1, 1, 0.1),
    'num_round': hp.quniform('num_round', 5, 10, 5),
    'nthread': 1,
    'silent': 1,
    'seed': 111,
    "max_evals": 200,
}


X_train, X_test, y_train, y_test = train_test_split(df_train, y, test_size=0.2, random_state=1234)

trials = Trials()
best = fmin(score, params, algo=tpe.suggest, trials=trials, max_evals=5)

xg_train = xgb.DMatrix(df_train, label=y)
clf = xgb.train(best, xg_train, 100)

y_pred = clf.predict(xgb.DMatrix(df_test))
for i in range(len(y_pred)):
    if y_pred[i]<1.0:
        y_pred[i] = 1.0
    if y_pred[i]>3.0:
        y_pred[i] = 3.0


pd.DataFrame({"id": df_test_id.values.ravel(), "relevance": y_pred}).to_csv('submission_xgb.csv',index=False)
