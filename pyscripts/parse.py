

dataDir="../data/"
trainFile,testFile, productDescp, attrFile ="train.csv", "test.csv", "product_descriptions.csv", "attributes.csv"
files = [trainFile, testFile, productDescp, attrFile]

def parse():
    """
    parses data from file into python holders
    """
    alldata=[]
    for filef in files:
        filepath = dataDir + filef
        data = [x.strip().replace("\"", "").split(',') for x in open(filepath, 'r').readlines()]
        print len(data)
        alldata.append(data)
    return alldata

if __name__ == "__main__":
    trainData, testData, productData, attrData = parse()
