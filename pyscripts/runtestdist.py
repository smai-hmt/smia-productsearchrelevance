import numpy as np
import pandas as pd
from genFeat_distance_feat import *
from genFeat_counting_feat import *
import sys
import cPickle
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from helper import join_attrs

# Read the csv files
#df_test = pd.read_csv('../data/train.csv')
df_test = pd.read_csv('../data/test.csv')
df_attr = pd.read_csv('../data/attributes.csv')
df_pro_desc = pd.read_csv('../data/product_descriptions.csv')

#df_test = df_test.join(df_pro_desc, on = 'product_uid', how = 'left',rsuffix='pid2')
df_test = pd.merge(df_test, df_pro_desc, how='left', on='product_uid')
df_test = join_attrs(df_test,df_attr)

for i in range(0,df_test.shape[0],10000):
    to = min(df_test.shape[0],i+10000)
    df2 = df_test.loc[range(i,to),:]
    run_dist_features(df2)

    ''' Extract basic distance features '''

    feat_names = [name for name in df2.columns if "jaccard_coef" in name or "dice_dist" in name]
    toDrop = [
           orig_feat for orig_feat in df2.columns \
               if orig_feat not in feat_names
    ]

    df2 = df2.drop(toDrop,axis=1)
    df2.to_csv('XXX'+str(i), sep=',',index=False,header=None)
