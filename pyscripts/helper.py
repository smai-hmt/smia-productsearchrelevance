import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from nltk.stem.snowball import SnowballStemmer

#print("reading data...")
#df_train = pd.read_csv('data/train.csv')
#df_test = pd.read_csv('data/test.csv')
# df_attr = pd.read_csv('../input/attributes.csv')
#df_pro_desc = pd.read_csv('data/product_descriptions.csv')

def concate_attrs(attrs):
    """
    attrs is all attributes of the same product_uid
    """
    names = attrs["name"]
    values = attrs["value"]
    pairs  = []
    for n, v in zip(names, values):
        pairs.append(' '.join((n, v)))
    return ' '.join(pairs)


def join_attrs(df,attributes):

    print("concat attributes...")
    attributes.dropna(how="all", inplace=True)
    # attributes[attributes.product_uid.isnull()]
    attributes["product_uid"] = attributes["product_uid"].astype(int)
    attributes["value"] = attributes["value"].astype(str)

    product_attrs = attributes.groupby("product_uid").apply(concate_attrs)
    product_attrs = product_attrs.reset_index(name="product_attributes")

    df = pd.merge(df, product_attrs, how="left", on="product_uid")
    df['product_attributes'] = df['product_attributes'].fillna('')

    return df
