
"""
__file__

    genFeat_distance_feat.py

__description__

    This file generates the following features for each run and fold, and for the entire training and testing set.

        1. jaccard coefficient/dice distance between query & title, query & description, title & description pairs
            - just plain jaccard coefficient/dice distance
            - compute for unigram/bigram/trigram

        2. jaccard coefficient/dice distance stats features for title/description
            - computation is carried out with regard to a pool of samples grouped by:
                - sample title        vs.  pooled sample titles
                - sample description  vs.  pooled sample descriptions
                Note that in the pool samples, we exclude the current sample being considered.
            - stats features include quantiles of cosine similarity and others defined in the variable "stats_func", e.g.,
                - standard deviation (std)
                - more can be added, e.g., moment features etc

__author__

    Chenglong Chen < c.chenglong@gmail.com >

"""

import re
import sys
import ngram
import cPickle
import numpy as np
import pandas as pd
from copy import copy
from nlp_utils import stopwords, english_stemmer, stem_tokens
from feat_utils import try_divide, get_sample_indices_by_relevance, dump_feat_name
sys.path.append("../")
from param_config import config

## stats feat is quite time-consuming to generate
## (about 2 days to generate on my computer)
stats_feat_flag = False


#####################
## Distance metric ##
#####################
def JaccardCoef(A, B):
    A, B = set(A), set(B)
    intersect = len(A.intersection(B))
    union = len(A.union(B))
    coef = try_divide(intersect, union)
    return coef

def DiceDist(A, B):
    A, B = set(A), set(B)
    intersect = len(A.intersection(B))
    union = len(A) + len(B)
    d = try_divide(2*intersect, union)
    return d

def compute_dist(A, B, dist="jaccard_coef"):
    if dist == "jaccard_coef":
        d = JaccardCoef(A, B)
    elif dist == "dice_dist":
        d = DiceDist(A, B)
    return d

#### pairwise distance
def pairwise_jaccard_coef(A, B):
    coef = np.zeros((A.shape[0], B.shape[0]), dtype=float)
    for i in range(A.shape[0]):
        for j in range(B.shape[0]):
            coef[i,j] = JaccardCoef(A[i], B[j])
    return coef
    
def pairwise_dice_dist(A, B):
    d = np.zeros((A.shape[0], B.shape[0]), dtype=float)
    for i in range(A.shape[0]):
        for j in range(B.shape[0]):
            d[i,j] = DiceDist(A[i], B[j])
    return d

def pairwise_dist(A, B, dist="jaccard_coef"):
    if dist == "jaccard_coef":
        d = pairwise_jaccard_coef(A, B)
    elif dist == "dice_dist":
        d = pairwise_dice_dist(A, B)
    return d


'''
def test():

    print s1,s2
    
    print 'jacc'
    print compute_dist(s1,s2)

    print 'dice'
    print compute_dist(s1,s2,'dice_dist')
    print 'pairwise jacc'
    print pairwise_dist(s1,s2)

    print 'pairwise dice'
    print pairwise_dist(s1,s2,'dice_dist')
'''

'''
######################
## Pre-process data ##
######################
token_pattern = r"(?u)\b\w\w+\b"
#token_pattern = r'\w{1,}'
#token_pattern = r"\w+"
#token_pattern = r"[\w']+"
transform = config.count_feat_transform
def preprocess_data(line, token_pattern=token_pattern,
                     exclude_stopword=config.cooccurrence_word_exclude_stopword,
                     encode_digit=False):
    token_pattern = re.compile(token_pattern, flags = re.UNICODE | re.LOCALE)
    ## tokenize
    tokens = [x.lower() for x in token_pattern.findall(line)]
    ## stem
    tokens_stemmed = stem_tokens(tokens, english_stemmer)
    if exclude_stopword:
        tokens_stemmed = [x for x in tokens_stemmed if x not in stopwords]
    return tokens_stemmed
'''

def preprocess_data(line):
    token_pattern = r"(?u)\b\w\w+\b"
    token_pattern = re.compile(token_pattern, flags = re.UNICODE | re.LOCALE)
    tokens = [x.lower() for x in token_pattern.findall(line)]
    tokens_stemmed = stem_tokens(tokens, english_stemmer)
    
    tokens_stemmed = [x for x in tokens_stemmed if x not in stopwords]
    return tokens_stemmed



#####################################
## Extract basic distance features ##
#####################################
def extract_basic_distance_feat(df):
    ## unigram
    print "generate unigram"
    #df["query_unigram"] = [preprocess_data(i[1]["search_term"]) for i in df.iterrows()]
    df["query_unigram"] = df["search_term"].map(lambda x: preprocess_data(x))
    df["title_unigram"] = df["product_title"].map(lambda x: preprocess_data(x))
    df["description_unigram"] = df["product_description"].map(lambda x: preprocess_data(x))
    df["attributes_unigram"] = df["product_attributes"].map(lambda x: preprocess_data(x))
    #df["title_unigram"] = [preprocess_data(i[1]["product_title"]) for i in df.iterrows()]
    #df["description_unigram"] = [preprocess_data(i[1]["product_description"]) for i in df.iterrows()]

    ## bigram
    print "generate bigram"
    join_str = "_"
    #df["query_bigram"] = list(df.apply(lambda x: ngram.getBigram(x["query_unigram"], join_str), axis=1))
    #df["title_bigram"] = list(df.apply(lambda x: ngram.getBigram(x["title_unigram"], join_str), axis=1))
    #df["description_bigram"] = list(df.apply(lambda x: ngram.getBigram(x["description_unigram"], join_str), axis=1))

    df["query_bigram"] = df["query_unigram"].map(lambda x:ngram.getBigram(x,join_str))
    df["title_bigram"] = df["title_unigram"].map(lambda x:ngram.getBigram(x,join_str))
    df["description_bigram"] = df["description_unigram"].map(lambda x:ngram.getBigram(x,join_str))
    df["attributes_bigram"] = df["attributes_unigram"].map(lambda x:ngram.getBigram(x,join_str))

    #df["query_bigram"] = [ngram.getBigram(i[1]["query_unigram"], join_str) for i in df.iterrows()]
    #df["title_bigram"] = [ngram.getBigram(i[1]["title_unigram"], join_str) for i in df.iterrows()]
    #df["description_bigram"] = [ngram.getBigram(i[1]["description_unigram"], join_str) for i in df.iterrows()]

    ## trigram
    print "generate trigram"
    join_str = "_"
    #df["query_trigram"] = [ngram.getTrigram(i[1]["query_unigram"], join_str) for i in df.iterrows()]
    #df["title_trigram"] = [ngram.getTrigram(i[1]["title_unigram"], join_str) for i in df.iterrows()]
    #df["description_trigram"] = [ngram.getTrigram(i[1]["description_unigram"], join_str) for i in df.iterrows()]

    df["query_trigram"] = df["query_unigram"].map(lambda x:ngram.getTrigram(x,join_str))
    df["title_trigram"] = df["title_unigram"].map(lambda x:ngram.getTrigram(x,join_str))
    df["description_trigram"] = df["description_unigram"].map(lambda x:ngram.getTrigram(x,join_str))
    df["attributes_trigram"] = df["attributes_unigram"].map(lambda x:ngram.getTrigram(x,join_str))

    ## jaccard coef/dice dist of n-gram
    print "generate jaccard coef and dice dist for n-gram"
    dists = ["jaccard_coef", "dice_dist"]
    grams = ["unigram", "bigram", "trigram"]
    feat_names = ["query", "title", "description","attributes"]
    for dist in dists:
        for gram in grams:
            for i in range(len(feat_names)-1):
                for j in range(i+1,len(feat_names)):
                    target_name = feat_names[i]
                    obs_name = feat_names[j]
                    df["%s_of_%s_between_%s_%s"%(dist,gram,target_name,obs_name)] = \
                            list(df.apply(lambda x: compute_dist(x[target_name+"_"+gram], x[obs_name+"_"+gram], dist), axis=1))

# Right now limit to basic features only


def run_dist_features(df):
    print 'starting distance features'
    extract_basic_distance_feat(df)
    print 'Done with distance'
