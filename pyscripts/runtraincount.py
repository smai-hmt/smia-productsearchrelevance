import numpy as np
import pandas as pd
from genFeat_distance_feat import *
from genFeat_counting_feat import *
import sys
import cPickle
from sklearn.ensemble import RandomForestRegressor, BaggingRegressor
from helper import join_attrs

sys.path.append('../pyscripts/')

# Read the csv files
df_train = pd.read_csv('../data/train.csv')
#df_test = pd.read_csv('../data/test.csv')
df_attr = pd.read_csv('../data/attributes.csv')
df_pro_desc = pd.read_csv('../data/product_descriptions.csv')

print df_train.shape

''' Do basic preproccesing, then go for advanced '''

df_train = pd.merge(df_train, df_pro_desc, how='left', on='product_uid')
df_train = join_attrs(df_train,df_attr)

for i in range(0,df_train.shape[0],10000):
    to = min(df_train.shape[0],i+10000)
    df2 = df_train.loc[range(i,to),:]
    run_counting_features(df2)

    print 'Done',i

    feat_names = [
            name for name in df2.columns \
                if "count" in name \
                or "ratio" in name \
                or "div" in name \
                or "pos_of" in name
        ]

    toDrop = [
            orig_feat for orig_feat in df2.columns \
                if orig_feat not in feat_names
        ]

    df2 = df2.drop(toDrop,axis=1)

    df2.to_csv('XXX'+str(i), sep=',',index=False,header=None)
