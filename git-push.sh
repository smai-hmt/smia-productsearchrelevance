#!/bin/bash


# give a custom comment as arg1
# e.g. ./git-push.sh "Done Task X"

echo git add -A
git add -A

commit_msg="`date` syncing"

if [ $# -ne 0 ]; then
    commit_msg="`date` $1"
fi

echo git commit -m \""`echo $commit_msg`"\"
git commit -m \""`echo $commit_msg`"\"

echo git push -u bitb master
git push -u bitb master
