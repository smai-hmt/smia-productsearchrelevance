#!/bin/bash
# Don't store anything in dataDir

dataDir="data/"
#mv $dataDir /tmp 2>/dev/null # safe to move to tmp than to delete
#mkdir -p $dataDir
#cd $dataDir

# getData, not re-runnable probably
#wget https://www.kaggle.com/c/home-depot-product-search-relevance/download/sample_submission.csv.zip
#wget https://www.kaggle.com/c/home-depot-product-search-relevance/download/train.csv.zip
#wget https://www.kaggle.com/c/home-depot-product-search-relevance/download/test.csv.zip
#wget https://www.kaggle.com/c/home-depot-product-search-relevance/download/product_descriptions.csv.zip
#wget https://www.kaggle.com/c/home-depot-product-search-relevance/download/attributes.csv.zip
#wget https://www.kaggle.com/c/home-depot-product-search-relevance/download/relevance_instructions.docx
#
files=(train.csv.zip test.csv.zip product_descriptions.csv.zip attributes.csv.zip)
#echo ${files[@]}

if [[ -d $dataDir ]]; then
    echo "Data directory found"
    for f in ${files[@]}; do
        if [[ -f $dataDir"/"$f ]]; then
            echo "found $dataDir$f"
        else
            echo "$dataDir$f not found, exiting further validations"
            exit 0
        fi
    done

else
    echo "$dataDir not found, exiting further validations"
    exit 0
fi

echo "All Data files present"
cd $dataDir 1>/dev/null
for f in ${files[@]}; do
    unzip -o $f 1>/dev/null
done

echo "Unzipped all files"
mkdir -p zip
mv *.zip zip
cd - 1>/dev/null

echo "Sanitizing Data"
echo "1. Removing 155 blank lines from attributes"
sed -i -e '/^,/d' $dataDir"/"${files[3]%%.zip}

echo "2. Replacing comman within quotes only, so that line.split() works painlessly in python"

cd $dataDir 1>/dev/null
for f in ${files[@]}; do
    fileName=${f%%.zip}
    awk -F'"' -v OFS='' '{ for (i=2; i<=NF; i+=2) gsub(",", "", $i) } 1' $fileName > "_"$fileName
    mv $fileName $fileName".bak"
    mv "_"$fileName $fileName
done
cd - 1>/dev/null


echo "Setup Done"
